package com.parse.starter.util;

import java.util.ArrayList;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class ToggleClass implements OnClickListener {

	private ArrayList<View> toogleViews = new ArrayList<View>();
	public int currentlySelected = -1;
	private ToggleSelection listener;

	public ArrayList<View> getToogleViews() {
		return toogleViews;
	}

	public void setToogleViews(ArrayList<View> toogleViews) {
		this.toogleViews = toogleViews;
	}

	public int getCurrentlySelected() {
		return currentlySelected;
	}

	public void setCurrentlySelected(int currentlySelected) {
		this.currentlySelected = currentlySelected;
	}

	public ToggleSelection getListener() {
		return listener;
	}

	public void setListener(ToggleSelection listener) {
		this.listener = listener;
	}

	public ToggleClass(ToggleSelection listener) {
		this.listener = listener;
	}

	public void addView(View view) {
		addView(view, false);
	}

	public void addView(View view, boolean isSelected) {
		toogleViews.add(view);
		view.setOnClickListener(this);
		view.setTag(toogleViews.size() - 1);
		if (isSelected) {
			setSelection(toogleViews.size() - 1);
		}

	}

	public void setSelection(int index) {
		if (currentlySelected != -1) {
			setUnselection(currentlySelected);
		}
		if (listener != null) {
			listener.onToggleSelection(index, toogleViews.get(index));
			currentlySelected = index;
		}
	}

	public void setUnselection(int index) {
		Log.i("Clicked index", index + "");
		if (listener != null)
			listener.onToggleUnselected(index, toogleViews.get(index));
	}

	@Override
	public void onClick(View v) {
		int index = (Integer) v.getTag();
		Log.i("Clicked index", index + "");
		if (index != currentlySelected) {
			setSelection(index);
		}
	}

	public interface ToggleSelection {
		public void onToggleSelection(int index, View v);

		public void onToggleUnselected(int index, View v);
	}

}
