package com.parse.starter.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class Util {

	public interface DialogListener {
		public void onOKPressed(DialogInterface dialog, int which);

		public void onCancelPressed(DialogInterface dialog, int which);
	}

	public static AlertDialog createAlertDialog(Context context,
			String message, String title, boolean isCancelable, String okText,
			String cancelText, final Util.DialogListener listener) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message);
		builder.setTitle(title);

		builder.setCancelable(isCancelable);
		builder.setPositiveButton(okText,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						listener.onOKPressed(dialog, which);
					}
				});
		builder.setNegativeButton(cancelText,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						listener.onCancelPressed(dialog, which);
					}
				});

		return builder.create();
	}

	public static boolean isConnectingToInternet(Context ctx) {

		boolean NetConnected = false;
		try {
			ConnectivityManager connectivity = (ConnectivityManager) ctx
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity == null) {
				NetConnected = false;
			} else {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null) {
					for (int i = 0; i < info.length; i++) {
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							NetConnected = true;
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Connectivity Exception", "Exception AT isInternetConnection");
			NetConnected = false;
		}
		return NetConnected;

	}

	public static String getStringFromInputStream(InputStream is) {
		StringBuilder response = new StringBuilder();
		try {
			BufferedReader buReader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"), 50000);

			String line;

			while ((line = buReader.readLine()) != null) {
				response.append(line);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response.toString();

	}

	public static void startItemActivity(Context ctx, Class activityClass) {
		Intent i = new Intent(ctx, activityClass);
		ctx.startActivity(i);
	}

	public static String getStringFromHTMLContent(String s) {
		String str = s.replaceAll("<br />", "<br /><br />").replaceAll(
				"&nbsp;", "<br /><br />");
		Log.e("String After", str);
		return str;
	}

	public static String convertDateFormat(String currentDate,
			String reqDateFormat) {
		SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat format = new SimpleDateFormat(reqDateFormat);
		try {
			Date d = currentDateFormat.parse(currentDate);
			return format.format(d);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return currentDate;
	}

}
