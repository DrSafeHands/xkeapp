package com.parse.starter.util;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Adapter;

import com.parse.starter.db.DBAdapter;
import com.parse.starter.db.DBConstants;

public class Session implements Serializable, Comparable<Session> {

	public String sessionTitle = "sessionTitle";

	public String speakersNames = "";

	public String sessionDate = "";
	public String sessionStartTime = "";
	public String sessionEndTime = "";
	public String SessionID = "";
	

	public static void parseJSONString(String jsonString, Context ctx) {
		DBAdapter adapter = new DBAdapter(ctx);
		adapter.open();
		try {

			JSONObject obj = new JSONObject(jsonString);
			JSONArray array = obj.optJSONArray("results");
			if (array != null && array.length() > 0) {
				adapter.deleteAllItemsFromTable(DBConstants.TABLE_XKE_SESSIONS);
				for (int i = 0; i < array.length(); i++) {
					JSONObject childObj = array.getJSONObject(i);
					Session s = new Session();
					s.sessionDate = childObj
							.optString(DBConstants.COL_sessionDate);
					s.sessionTitle = childObj
							.optString(DBConstants.COL_sessionTitle);
					s.sessionStartTime = childObj
							.optString(DBConstants.COL_sessionStartTime);
					s.sessionEndTime = childObj
							.optString(DBConstants.COL_sessionEndTime);
					s.SessionID = childObj.optString(DBConstants.COL_sessionID);
					s.speakersNames = childObj
							.optString(DBConstants.COL_speakerNames);
					adapter.insertSerializableObject(
							DBConstants.TABLE_XKE_SESSIONS, s);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		adapter.close();
	}

	public Date convertStringToDate(String date, String startTime) {
		SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm a");
		try {
			Date d = format.parse(date + " " + startTime);
			return d;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public int compareTo(Session another) {
		Date date1 = this.convertStringToDate(this.sessionDate,
				this.sessionStartTime);
		Date date2 = another.convertStringToDate(another.sessionDate,
				another.sessionStartTime);
		try {
			return date1.compareTo(date2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}
