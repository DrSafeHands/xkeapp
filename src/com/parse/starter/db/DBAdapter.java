package com.parse.starter.db;

import java.io.IOException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.parse.starter.SharedPreferenceUtil;

public class DBAdapter implements DBConstants {

	private static final String TAG = "DBAdapter";
	private static final String DATABASE_NAME = "eventsapp";
	private static final int DATABASE_VERSION = 8;

	private final Context context;
	private DatabaseHelper DBHelper;
	public SQLiteDatabase db;

	public DBAdapter(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {
		Context ctx;

		DatabaseHelper(Context context) {

			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			ctx = context;

		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			try {
				db.execSQL(DBConstants.CREATE_BLOB_XKE_FEEDBACK);
				db.execSQL(DBConstants.CREATE_BLOB_XKE_SESSIONS);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + DBConstants.TABLE_XKE_Feedback);
			db.execSQL("DROP TABLE IF EXISTS " + DBConstants.TABLE_XKE_SESSIONS);
			SharedPreferenceUtil.getInstance(ctx).saveData("userPhoneData",
					false);
			onCreate(db);
		}
	}

	public long insertSerializableObject(String tableName, Object object) {
		ContentValues values = new ContentValues();

		try {
			values.put(DBConstants.COL_SERIALIZE_OBJECT,
					Serializer.serialize(object));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return db.insert(tableName, null, values);
	}

	public long updateSerializableObject(String tableName, Object object,
			int _id) {
		ContentValues values = new ContentValues();

		try {
			values.put(DBConstants.COL_SERIALIZE_OBJECT,
					Serializer.serialize(object));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return db.update(tableName, values, DBConstants._ID + " =" + _id, null);
	}

	public void deleteAllItemsFromTable(String tableName) {
		db.execSQL("delete  from " + tableName);
	}

	// ---opens the database---
	public DBAdapter open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	public Cursor executeRawQuery(String TableName, String columnName,
			String value) {
		return db.rawQuery("select * from " + TableName + " where "
				+ columnName + " = ?", new String[] { value });
	}

	public Cursor executeRawQuery(String query, String[] selectionArgs) {
		return db.rawQuery(query, selectionArgs);
	}

}
