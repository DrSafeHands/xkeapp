package com.parse.starter.db;

public interface DBConstants {

	String _ID = "_id";
	String COL_SERIALIZE_OBJECT = "colSerializableObject";
	String COL_sessionTitle = "sessionTitle";
	String COL_sessionID = "sessionID";
	String COL_speakerNames = "speakerNames";
	String COL_sessionStartTime = "sessionStartTime";
	String COL_sessionEndTime = "SessionEndTime";
	String COL_sessionDate = "sessionDate";
	String TABLE_XKE_SESSIONS = "xkeSession";
	
	String TABLE_XKE_Feedback= "sessionFeedback";
	String COL_ratingPoint= "ratingPoint";
	String COL_xebiaID = "xebiaID";
	String COL_username = "username";
	String COL_comments = "comments";

	String Col_Session_isAddedToSchedule = "isAddedToSchedule";

	String CREATE_BLOB_XKE_SESSIONS = "create table " + TABLE_XKE_SESSIONS
			+ " ( " + COL_SERIALIZE_OBJECT + " BLOB, " + _ID
			+ " integer primary key autoincrement)";
	
	String CREATE_BLOB_XKE_FEEDBACK = "create table " + TABLE_XKE_Feedback
			+ " ( " + COL_SERIALIZE_OBJECT + " BLOB, " + _ID
			+ " integer primary key autoincrement)";

	String YES = "YES";
	String NO = "NO";
	
	String APPLICATION_ID = "wX7yFa9vn2d6dCGhtC1TBJYz90B9y1iG2mNwL829";
	String REST_API_ID = "FWYtV1l1EK5O8vNxSjXg760qfe6Oo50BuJV5oyKp";
	String CLIENT_ID = "zXMges8M3p598g8lKzmzRVJELeYR42S3Fk8nZgnY";

}
