package com.parse.starter;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.starter.db.DBAdapter;
import com.parse.starter.db.DBConstants;
import com.parse.starter.db.Serializer;
import com.parse.starter.util.HTTPConnection;
import com.parse.starter.util.Session;

public class XKESessionsList extends Activity implements
		CustomListAdapterInterface, OnItemClickListener, OnClickListener {

	ListView list;
	ArrayList<Session> sessions = new ArrayList<Session>();;
	CustomListAdapter<Session> adapter;
	int listCount = 0;
	ProgressDialog dialog;
	String dialogText = "Refreshing Data...";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_xkesessions);
		list = (ListView) findViewById(R.id.listView_sessions);
		loadData();
		adapter = new CustomListAdapter<Session>(this,
				R.layout.row_session_item, sessions, this);
		list.setAdapter(adapter);
		findViewById(R.id.emptyView).setOnClickListener(this);
		list.setEmptyView(findViewById(R.id.emptyView));
		list.setOnItemClickListener(this);
		initCustomActionBar();
	}

	private void initCustomActionBar() {
		findViewById(R.id.iv_backButton).setVisibility(View.INVISIBLE);
		TextView title = (TextView) findViewById(R.id.tv_actionBarTitle);
		title.setText("XKE Sessions");
		findViewById(R.id.iv_actionBarRefresh).setOnClickListener(this);
	}

	private void loadData() {
		// TODO Auto-generated method stub
		sessions.clear();
		DBAdapter adapter = new DBAdapter(this);
		adapter.open();
		String query = "select * from " + DBConstants.TABLE_XKE_SESSIONS;
		Cursor c = adapter.executeRawQuery(query, null);
		while (c.moveToNext()) {
			try {
				Session s = (Session) Serializer.deserialize(c.getBlob(c
						.getColumnIndex(DBConstants.COL_SERIALIZE_OBJECT)));
				sessions.add(s);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		adapter.close();
		listCount = sessions.size();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	private void downloadData() {
		String url = "https://api.parse.com/1/classes/"
				+ DBConstants.TABLE_XKE_SESSIONS;
		Task task = new Task();
		task.execute(url);
	}

	private void showDialog() {
		dialog = new ProgressDialog(this);
		dialog.setMessage(dialogText);
		dialog.setTitle("Please Wait!");
		dialog.setCancelable(false);
		dialog.show();

	}

	private class Task extends AsyncTask {

		protected void onPreExecute() {
			showDialog();
		};

		@Override
		protected Object doInBackground(Object... params) {
			// TODO Auto-generated method stub
			String url = (String) params[0];
			String json = HTTPConnection.getJSONFromUrl(url);
			Log.e("JSON received", json);
			Session.parseJSONString(json, XKESessionsList.this);
			return null;
		}

		protected void onPostExecute(Object result) {
			dialog.dismiss();
			loadData();
			adapter.notifyDataSetChanged();
		};

	};

	@Override
	public View getView(int position, View view, ViewGroup parent,
			int resourceID) {
		ViewHolder holder = new ViewHolder();
		if (view == null) {
			view = LayoutInflater.from(this).inflate(resourceID, null);
			holder.sTime = (TextView) view
					.findViewById(R.id.row_session_item_sessionTime);
			holder.sName = (TextView) view
					.findViewById(R.id.row_session_item_speakerName);
			holder.sTitle = (TextView) view
					.findViewById(R.id.row_session_item_sessionTitle);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		Session s = sessions.get(position);
		holder.sTime.setText(s.sessionDate + ", " + s.sessionStartTime + "-"
				+ s.sessionEndTime);
		holder.sName.setText(s.speakersNames);
		holder.sTitle.setText(s.sessionTitle);
		return view;
	}

	private class ViewHolder {
		TextView sTitle, sTime, sName;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {
		// TODO Auto-generated method stub
		Session s = sessions.get(index);
		Log.e("Session ID", s.SessionID + "");
		Bundle b = new Bundle();
		b.putSerializable(CommonConstants.BUNDLE_KEY_SESSION_OBJECT, s);
		Intent i = new Intent(this, XKERatingActivity.class);
		i.putExtras(b);
		startActivity(i);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.emptyView:
			downloadData();
			dialogText = "Downloading Data...";
			break;
		case R.id.iv_actionBarRefresh:
			downloadData();
			dialogText = "Refreshing Data...";
			break;
		default:
			break;
		}
	}

}
