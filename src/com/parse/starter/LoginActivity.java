package com.parse.starter;

import java.util.HashMap;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity implements OnClickListener {
	EditText pwd, username;
	Button register, login;
	TextView errorView;
	String errorText = "";
ProgressDialog dialog ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_screen_new);
		if (SharedPreferenceUtil.getInstance(this).getData("LOGIN_KEY", false)) {
			startNextActivity(XKESessionsList.class);
		} else {
			username = (EditText) findViewById(R.id.et_userName);
			pwd = (EditText) findViewById(R.id.et_passWord);
			register = (Button) findViewById(R.id.btn_register);
			login = (Button) findViewById(R.id.btn_signIN);
			errorView = (TextView) findViewById(R.id.tv_loginError);
			register.setOnClickListener(this);
			login.setOnClickListener(this);
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_signIN:
			String userName = username.getText().toString();
			String passWord = pwd.getText().toString();
			if (username.equals("") || passWord.equals("")) {
				errorText = "Username/Password can not be empty";
			}  else {
				validateUser(userName, passWord);

			}
			errorView.setText(errorText);
			errorView.setVisibility(View.VISIBLE);
			break;
		case R.id.btn_register:
			startNextActivity(RegisterActivity.class);
			break;
		default:
			break;
		}
	}
	
	private void validateUser(final String user, final String pwd){
		
		showDialog();
		
		HashMap<String , Object> map = new HashMap<String, Object>();
		
		map.put(CommonConstants.USERNAME, user);
		map.put(CommonConstants.PASSWORD, pwd);
		
		ParseCloud.callFunctionInBackground(CommonConstants.CLASS_LOGIN, map, new FunctionCallback<String>() {
			  public void done(String result, ParseException e) {
				  dialog.dismiss();
			    if (e == null) {
			      // result is "Hello world!"
			    	Log.i("Result is", result+"");
			    	if(result.equals(CommonConstants.RESULT_LOGIN_SUCCESSFUL)){
			    		
			    		SharedPreferenceUtil prefs = SharedPreferenceUtil
								.getInstance(LoginActivity.this);
						prefs.saveData(CommonConstants.LOGIN_KEY, true);
						prefs.saveData(CommonConstants.USERNAME, user);
						prefs.saveData(CommonConstants.PASSWORD, pwd);
						
						Intent i = new Intent(LoginActivity.this,
								XKESessionsList.class);
						
						startActivity(i);
						
						finish();
			    		
			    	}else{
			    		errorView.setText("Invalid Username or Password");
			    		errorView.setVisibility(View.GONE);
			    	}
			    }else{
			    	e.printStackTrace();
			    }
			  }
			});
	}
	
	private void showDialog(){
		dialog = new ProgressDialog(this);
		dialog.setTitle("Please Wait");
		dialog.setMessage("Verifying User...");
		dialog.setCancelable(false);
		dialog.show();
	}

	private void startNextActivity(Class actvitiy) {
		Intent i = new Intent(this, actvitiy);
		startActivity(i);
		finish();
	}

}
