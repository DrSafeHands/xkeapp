package com.parse.starter;

import java.io.IOException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.parse.starter.db.DBAdapter;
import com.parse.starter.db.DBConstants;
import com.parse.starter.db.Serializer;
import com.parse.starter.util.Session;
import com.parse.starter.util.ToggleClass;
import com.parse.starter.util.ToggleClass.ToggleSelection;
import com.parse.starter.util.Util;
import com.parse.starter.util.XKEFeedback;

public class XKERatingActivity extends Activity implements ToggleSelection,
		OnClickListener {
	Session session;
	ToggleClass toggleButtons;
	ProgressDialog dialog;
	EditText comments;
	XKEFeedback f;

	private void initCustomActionBar() {
		findViewById(R.id.iv_actionBarRefresh).setVisibility(View.INVISIBLE);
		TextView title = (TextView) findViewById(R.id.tv_actionBarTitle);
		title.setText("Session Feedback");
		findViewById(R.id.iv_backButton).setOnClickListener(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback_page);

		session = (Session) getIntent().getExtras().getSerializable(
				CommonConstants.BUNDLE_KEY_SESSION_OBJECT);
		setText(R.id.tv_sessionTime, session.sessionDate + ", "
				+ session.sessionStartTime + " - " + session.sessionEndTime);
		setText(R.id.tv_speakerName, session.speakersNames);
		setText(R.id.tv_sessionTitle, session.sessionTitle);

		toggleButtons = new ToggleClass(this);
		toggleButtons.addView(findViewById(R.id.ll_awesome));
		toggleButtons.addView(findViewById(R.id.ll_good));
		toggleButtons.addView(findViewById(R.id.ll_poor));
		Log.e("XKE feedback", "sessionID=" + session.SessionID);
		Button feedBackButton = (Button) findViewById(R.id.btn_submit_feedback);
		comments = (EditText) findViewById(R.id.et_comments);

		f = getFeedbackObject(session.SessionID);
		if (f != null) {
			comments.setText(f.comments);
			toggleButtons.setSelection(f.ratingPoint - 5);
			feedBackButton.setText("Feedback Already Submitted");
			feedBackButton.setEnabled(false);
			toggleButtons.setListener(null);
		} else {

			feedBackButton.setOnClickListener(this);
		}
		Log.e("Feedback Object", f + "");
		initCustomActionBar();
	}

	private XKEFeedback getFeedbackObject(String sessionID) {
		DBAdapter adapter = new DBAdapter(this);
		adapter.open();
		String query = "select * from " + DBConstants.TABLE_XKE_Feedback;
		Cursor c = adapter.executeRawQuery(query, null);
		while (c.moveToNext()) {
			try {
				Log.e("Cursor Count	", c.getCount() + "");

				XKEFeedback feedback = (XKEFeedback) Serializer
						.deserialize(c.getBlob(c
								.getColumnIndex(DBConstants.COL_SERIALIZE_OBJECT)));
				if (sessionID.equals(feedback.sessionID)) {
					adapter.close();
					return feedback;
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		adapter.close();
		return null;
	}

	private void setText(int textViewID, String text) {
		TextView tv = (TextView) findViewById(textViewID);
		tv.setText(text);
	}

	@Override
	public void onToggleSelection(int index, View v) {
		v.setBackgroundResource(android.R.drawable.picture_frame);
		TextView tv = (TextView) v.findViewWithTag("ratingTextView");
		tv.setTextColor(getResources().getColor(R.color.text_black));
		tv.setTypeface(null, Typeface.BOLD);
	}

	@Override
	public void onToggleUnselected(int index, View v) {
		// TODO Auto-generated method stub
		v.setBackgroundResource(0);
		TextView tv = (TextView) v.findViewWithTag("ratingTextView");
		tv.setTextColor(getResources().getColor(R.color.text_view_grey));
		tv.setTypeface(null, Typeface.NORMAL);
	}

	private void showDialog() {
		dialog = new ProgressDialog(this);
		dialog.setMessage("Submitting Feedback...");
		dialog.setTitle("Please Wait!");
		dialog.setCancelable(false);
		dialog.show();

	}

	private void dismiss() {
		dialog.dismiss();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_submit_feedback:
			if (Util.isConnectingToInternet(this)) {
				if (toggleButtons.currentlySelected != -1) {
					findViewById(R.id.tv_errorRatingView).setVisibility(
							View.GONE);
					showDialog();
					uploadFeedback();

				} else {
					findViewById(R.id.tv_errorRatingView).setVisibility(
							View.VISIBLE);
				}

			} else {
				Toast.makeText(this, "Currently not connected to internet",
						Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.iv_backButton:
			onBackPressed();
			break;
		default:
			break;
		}
	}

	public void uploadFeedback() {
		ParseObject obj = new ParseObject(DBConstants.TABLE_XKE_Feedback);
		final int points = 5 - toggleButtons.currentlySelected;
		obj.put(DBConstants.COL_ratingPoint, points);
		final String commentText = comments.getText().toString();
		obj.put(DBConstants.COL_comments, commentText);
		final String userName = SharedPreferenceUtil.getInstance(this).getData(
				CommonConstants.USERNAME, "");
		final String xebiaID = SharedPreferenceUtil.getInstance(this).getData(
				CommonConstants.XEBIA_ID, "");
		obj.put(DBConstants.COL_username, userName);
		obj.put(DBConstants.COL_xebiaID, xebiaID);

		obj.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				if (e == null) {
					if (f == null) {
						f = new XKEFeedback();

					}
					f.comments = commentText;
					f.ratingPoint = points;
					f.username = userName;
					f.xebiaID = xebiaID;
					f.sessionID = session.SessionID;
					saveFeedbackToPreferences(f);

				} else {
					e.printStackTrace();
					Toast.makeText(XKERatingActivity.this,
							"Unable to submit feedback, please try again",
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	private void saveFeedbackToPreferences(XKEFeedback f) {
		DBAdapter adapter = new DBAdapter(XKERatingActivity.this);
		adapter.open();
		/*
		 * if (f._id != -1) {
		 * adapter.updateSerializableObject(DBConstants.TABLE_XKE_Feedback, f,
		 * f._id); } else {
		 */
		long inserted = adapter.insertSerializableObject(
				DBConstants.TABLE_XKE_Feedback, f);
		Log.i("Inserted index", inserted + "");
		// }
		adapter.close();
		XKERatingActivity.this.finish();
	}

}
