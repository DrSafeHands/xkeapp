package com.parse.starter;

public interface CommonConstants {
	String LOGIN_KEY = "LOGIN_KEY";
	String USERNAME = "username";
	String PASSWORD = "password";
	String XEBIA_ID = "xebiaID";

	String CLASS_LOGIN = "login";
	String RESULT_LOGIN_SUCCESSFUL = "Login Successful";
	String RESULT_LOGIN_FAILED = "Login FAILED";
	
	String BUNDLE_KEY_SESSION_OBJECT = "BUNDLE_KEY_SESSION_OBJECT";
}
