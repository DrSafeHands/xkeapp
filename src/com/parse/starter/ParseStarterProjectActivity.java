package com.parse.starter;

import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.parse.FunctionCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseCloud;
import com.parse.ParseException;

public class ParseStarterProjectActivity extends Activity {
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_screen_new);

		ParseAnalytics.trackAppOpened(getIntent());
		HashMap<String , Object> map = new HashMap<String, Object>();
		map.put("username", "nitin");
		map.put("password", "123456");
		ParseCloud.callFunctionInBackground("login", map, new FunctionCallback<String>() {
			  public void done(String result, ParseException e) {
			    if (e == null) {
			      // result is "Hello world!"
			    	Log.e("Result is", result+"");
			    }else{
			    	e.printStackTrace();
			    }
			  }
			});
		
	}
}
