package com.parse.starter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class RegisterActivity extends Activity implements OnClickListener {

	Button register;
	EditText xebiaID, userName, password;
	TextView errorView;
	ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		password = (EditText) findViewById(R.id.editText_reg_password);
		userName = (EditText) findViewById(R.id.editText_username);
		xebiaID = (EditText) findViewById(R.id.editText_xebiaID);
		register = (Button) findViewById(R.id.btn_reg_submit);
		errorView = (TextView) findViewById(R.id.tv_regError);

		register.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_reg_submit:
			String user = userName.getText().toString();
			String xID = xebiaID.getText().toString();
			String pwd = password.getText().toString();

			if (user.equals("") || pwd.equals("")) {
				errorView.setText("Username/Password can not be empty");
				errorView.setVisibility(View.VISIBLE);

			} else {
				errorView.setVisibility(View.GONE);
				login(user, xID, pwd);
			}

			break;

		default:
			break;
		}
	}

	private void login(final String user, final String xID, final String pwd) {
		showDialog();
		ParseObject obj = new ParseObject("login");
		obj.put("username", user);
		obj.put("password", pwd);
		obj.put("xebiaID", xID);
		obj.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				if (e == null) {
					
					Intent i = new Intent(RegisterActivity.this,
							LoginActivity.class);
					startActivity(i);
					finish();
				} else {
					errorView
							.setText("Some unknown Error occurred while registering user, Please try again");
					errorView.setVisibility(View.VISIBLE);
				}
			}
		});
	}
	
	private void showDialog(){
		dialog = new ProgressDialog(this);
		dialog.setMessage("Registering User");
		dialog.setTitle("Please Wait!");
		dialog.setCancelable(false);
		dialog.show();

	}

}
